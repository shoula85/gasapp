import React from 'react';
import PropTypes from 'prop-types';
import './Notifications.scss';

function Notifications({count}) {
    return (
        <div className="notifications">
            <p>My Gas Notifications</p>
            <div className="notifications__counter"><span>{count}</span></div>
            <span className="notifications__line" />
        </div>
    );
}

Notifications.propTypes = {
    count: PropTypes.number,
};

Notifications.defaultProps = {
    count: null,
};

export default Notifications;