import React from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';
import {Link} from "react-router-dom";

function Modal({title, children}) {
    return (
        <div className="modal">
            <div className="modal__header">
                <div className="modal__header__container">
                    <img className="modal__header__container__logo" src="/assets/images/gas-logo.png" />
                    <h1 className="modal__header__container__name">{title}</h1>
                </div>
                <Link to="/" className="modal__header__button">Go back</Link>
            </div>
            {children}
        </div>
    );
}

Modal.propTypes = {
    title: PropTypes.string,
    children: PropTypes.object,
};

Modal.defaultProps = {
    title: null,
    children: {},
};

export default Modal;