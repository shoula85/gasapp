import React from 'react';
import Modal from "../../organisms/Modal";
import Button from "../../atoms/Button";

function Add() {
    return (
        <Modal title="Gas Alerts">
            <div className="modal__content">
                <div className="modal__content__form">
                    <h1>Add Gas Notification</h1>
                    <Button title="Enable Notifications" />
                </div>
            </div>
        </Modal>
    );
}

export default Add;