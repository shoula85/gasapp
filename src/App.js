import {Routes, Route} from "react-router-dom";
import './App.scss';
import Home from "./components/pages/Home";
import Add from "./components/pages/Add";

function App() {
    return (
        <div className="app">
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/detail" element={<Add />} />
            </Routes>
        </div>
    );
}

export default App;
