import React from 'react';
import Tile from "../../molecules/Tile";
import {Link} from "react-router-dom";
import Notifications from "../../atoms/Notifications";
import Modal from "../../organisms/Modal";
import '../../atoms/Button/Button.scss';

function Home() {
    return (
        <Modal title="Gas Alerts">
            <div className="modal__content">
                <Tile title="Rapid" content="240" variant="main"/>
                <div className="modal__content__tiles">
                    <Tile title="FAST" content="210" variant="side-1"/>
                    <Tile title="STANDARD" content="150" variant="side-2"/>
                    <Tile title="SLOW" content="120" variant="side-3"/>
                </div>
                <p className="modal__content--bold">Last updated: 2 seconds ago</p>
                <Link to="/detail" className="button">Create a Gas Alert Notification</Link>
                <Notifications count={0} />
                <p><span className="modal__content--bold">You don’t have any gas notifications setup yet.</span><br />
                    Create one using the button above.</p>
            </div>
        </Modal>
    );
}

export default Home;