import React from 'react';
import PropTypes from 'prop-types';
import './Tile.scss';

function Tile({title, content, variant}) {
    return (
        <div className={"tile" + (variant ? ' tile--'+variant : '')}>
        <div className="tile__title">

            <h2>{title}</h2>
        </div>
            <div className="tile__content">{content}</div>
        </div>
    );
}

Tile.propTypes = {
    title: PropTypes.string,
    content: PropTypes.string,
    variant: PropTypes.string,
};

Tile.defaultProps = {
    title: null,
    content: null,
    variant: null,
};

export default Tile;